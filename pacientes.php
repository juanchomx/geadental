<?
  include("connection.php");
  include("gen_functions.php");
  session_start();

  if($_SESSION['loggedin'] == 0)
    die("<script>location.href = 'login.php'</script>");
  else{
    $iduser = $_SESSION['iduser'];
    $name = $_SESSION['name'];    
    $id_user_type = $_SESSION['id_user_type'];
    $idpanel = 2;///Pacientes
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>GEA Dental</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">
    <!-- Bootstrap css -->
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Libraries CSS Files -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/modal-video/css/modal-video.min.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">
    <!-- =======================================================
    Theme Name: eStartup
    Theme URL: https://bootstrapmade.com/estartup-bootstrap-landing-page-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
    ======================================================= -->
  </head>
  <body>
    <header id="header" class="header header-hide">
      <div class="container">
        <div id="logo" class="pull-left">
          <!-- <h1><a href="#body" class="scrollto"><span>e</span>Startup</a></h1> -->
          <!-- Uncomment below if you prefer to use an image logo -->
          <a href="#body"><img src="img/logo.png" alt="GEA Dental" title="" height="40" width="40" /></a>
        </div>
  
        <?
          ///Desplegar menu
         $menu = obtenerMenu($link, $id_user_type, $idpanel);
         if($menu != "")
          echo $menu;
        
        ?>
      </div>
      </header><!-- #header -->
      <!--==========================
      Features Section
      ============================-->
      <section id="features" class="padd-section text-center wow fadeInUp">
        <div class="container">
          <div class="section-title text-center">
            <h2>Dashboard</h2>
            <p class="separator">(Aquí van gráficas, indicadores y así)</p>
            <br>
            <a href="agregar_pacientes.php" class="btn  btn-primary"><i class="fa fa-address-card-o"></i> Agregar Paciente</a>
            <br><br>
			
			<h2 style="color:red;" hidden >Aplicacion en modo de pruebas</h2>
            
			<!-- Tabla de pacientes -->
            <table class="table table-striped table-dark table-hover table-bordered">
              <thead>
                <tr>
                  <th scope="col" hidden>ID Paciente</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Teléfono</th>
                  <th scope="col">Redes Sociales</th> 
                  <th scope="col">Agendar Cita</th>
                </tr>
              </thead>
              <tbody>
                <?
                  $qry="SELECT * FROM `geadental_hcc_info_personal`";

                  try {  
                    $result = mysqli_query($link, $qry);
                    
                    $rowCount=0;
                    while($row = $result->fetch_assoc()){
                      (int)$id_paciente = $row['id_paciente'];
                      $nombre = trim($row['nombre']);
                      $tel1 = trim($row['tel1']);
                      
					  if(trim($row['rrss']) == "")
						$rrss = "No tiene";
					  else
						$rrss = trim($row['rrss']);

                      echo '<tr>';
                        echo '<th scope="row" hidden>'.$id_paciente.'</th>';
                        ///echo "<th scope='row' class='col-md-1'><a href='loads_update.php?loadnum=" .$load_num. "'> ".$load_num.  '</a></th>';
                        echo '<td>'.$nombre.'</td>';
                        echo '<td>'.$tel1.'</td>';
                        echo '<td>'.$rrss.'</td>';
                        echo "<td> <button type='button' ><a href=agregar_citas.php?id_paciente=".$id_paciente." target='_blank'>Agedarle Cita</a></button> </td>";
                        ////echo "<td> <button type='button' ><a href=rpts/invoicerpt.php?load_num=".$loadNum." target='_blank'>Quick Print</a></button> </td>";
                        ////echo ''; ////http://syner.info/geadental/agregar_citas.php?id_paciente=0
                      echo '</tr>';
                      
                      $rowCount ++;
                    }
                    
                    if($rowCount == 0){
                    echo '<div class="alert alert-warning">';
                      echo '<p data-dismiss="alert">No hay pacientes registrados</p>';
                    echo '</div>';
                    }
                  }  
                  catch(exception $e) {
                    ///echo "ex: ".$e; 
                    echo '<div class="alert alert-warning">';
                    echo '<p data-dismiss="alert">Error al tratar de las lista de pacientes: '.$msg.'. Favor de contactar al administrador</p>';
                    echo '</div>';
                    mysql_close($link);
                    die();
                  }



                
                ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="container" align="center">
          <div id="lista_pacientes" class="row" >
            
          </div>
        </div>
      </section>
      <!-- Fin de la página-->
      <!-- Modals -->
      <!-- Modals -->
      <!--==========================
      Footer
      ============================-->
      <footer class="footer">
        <div class="container">
        </div>
          <div class="copyrights">
            <div class="container">
              <!-- <p>&copy; Copyrights eStartup. All rights reserved.</p> -->
              <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
                -->
                <!-- Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a> Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
                Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a>
              </div>
            </div>
          </div>
      </footer>
      <!-- JavaScript Libraries -->
      <script src="lib/jquery/jquery.min.js"></script>
      <script src="lib/jquery/jquery-migrate.min.js"></script>
      <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="lib/superfish/hoverIntent.js"></script>
      <script src="lib/superfish/superfish.min.js"></script>
      <script src="lib/easing/easing.min.js"></script>
      <script src="lib/modal-video/js/modal-video.js"></script>
      <script src="lib/owlcarousel/owl.carousel.min.js"></script>
      <script src="lib/wow/wow.min.js"></script>
      <!-- Contact Form JavaScript File -->
      <script src="contactform/contactform.js"></script>
      <!-- Template Main Javascript File -->
      <script src="js/main.js"></script>
    </body>
  </html>