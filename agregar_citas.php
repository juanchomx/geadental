<?
  include("connection.php");
  include("gen_functions.php");
  session_start();

  if($_SESSION['loggedin'] == 0)
    die("<script>location.href = 'login.php'</script>");
  else{
    $iduser = $_SESSION['iduser'];
    $name = $_SESSION['name'];    
    $id_user_type = $_SESSION['id_user_type'];
    $idpanel = 3;///Pacientes

    ///Paciente
    (int)$id_paciente = $_GET['id_paciente'];

    if($id_paciente > 0){
      $desp_lista_pacientes = 0;
      $qry = "SELECT * FROM `geadental_hcc_info_personal` WHERE id_paciente = ".$id_paciente." LIMIT 1";
    
    }else{
      $desp_lista_pacientes = 1;
      $qry = "SELECT * FROM `geadental_hcc_info_personal` ORDER BY nombre ASC";
    }
    
    try {       
      
      $result = mysqli_query($link, $qry);
      ///$row = $result1->fetch_assoc();
      
      ///echo '<br>qry: '.$qry.'<br>id_paciente: '.$id_paciente.'<br>';
      
    }  
    catch(exception $e) {
      ///echo "ex: ".$e; 
      $msg='Message: ' .$e->getMessage();
      
      echo '<div class="alert alert-warning">';
      echo '<p data-dismiss="alert">Error al tratar desplegar pacientes: '.$msg.'. Favor de contactar al administrador</p>';
      echo '<button><a href="index.php" ></a>Regresar</button>';
      echo '</div>';
      mysql_close($link);
      die();
      
    }
    ///$row = obtenerMultivalues($link, $qry);
    
    while ($row = $result->fetch_assoc()) {
      $id_paciente = $row['id_paciente'];
      $nombre = $row['nombre'];

      $arrIDspacientes[] = $id_paciente;
      $arrNombres[] = $nombre;

    }
    

    


  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>GEA Dental</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">
    <!-- Bootstrap css -->
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Libraries CSS Files -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/modal-video/css/modal-video.min.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">
    <!-- =======================================================
    Theme Name: eStartup
    Theme URL: https://bootstrapmade.com/estartup-bootstrap-landing-page-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
    ======================================================= -->
  </head>
  <body>
    <header id="header" class="header header-hide">
      <div class="container">
        <div id="logo" class="pull-left">
          <!-- <h1><a href="#body" class="scrollto"><span>e</span>Startup</a></h1> -->
          <!-- Uncomment below if you prefer to use an image logo -->
          <a href="#body"><img src="img/logo.png" alt="GEA Dental" title="" height="40" width="40" /></a>
        </div>
        
        <?
          ///Desplegar menu
         $menu = obtenerMenu($link, $id_user_type, $idpanel);
         if($menu != "")
          echo $menu;          
        ?>

      </div>
      </header><!-- #header -->
      <!--==========================
      Features Section
      ============================-->
      <section id="features" class="padd-section text-center wow fadeInUp">
        <div class="container">
          <div class="section-title text-center">
            <h2>Nueva cita</h2>
			<h3 style="color:red" >Función en modalidad de pruebas</h3>
            <br><br>

            <?
              ///echo '<br>id_paciente: '.$id_paciente.'<br>';
            ?> 

            <!-- Formulario: agrgar pacientes -->
			<form method="post" action="dbcrud1.php">

              <!-- Paciente y Fecha -->
              <h3>Paciente y Fecha de Cita</h3>
              <input type="text" class="form-control col-sm-6" id="id_med" name="id_med"  value="<? echo $iduser ?>" hidden>
              <input type="text" class="form-control col-sm-6" id="id_paciente_txt" name="id_paciente_txt"  value="<? echo $id_paciente ?>" hidden>
              <div id="ip_ID" class="estilo1">
                  <div class="form-row">
                    <div class="col">
                      <input type="text" class="form-control  col-sm-8" id="cant_pacientes" name="cant_pacientes" value="<? echo $cant_pacientes ?>" hidden>
					  <label for="inputAddress">Paciente</label ><br><!--&nbsp; &nbsp;<label id="msgs" class="warningmsg1" hidden></label><br> -->
                      <!-- <input type="text" class="form-control col-sm-6" id="nombre_pac" name="nombre_pac"  placeholder="nombre paciente" required  > -->
                      <?
                        
						if($desp_lista_pacientes){
						  echo '<select class="form-control col-sm-8" id="nombre_pac_sel" name="nombre_pac_sel" onchange="HabilitaNombre()" >';
                          $arrIndx = 0;
						  echo '<option value="'.($cant_pacientes + 1).'" >S/Expediente</option>';
                          foreach($arrNombres as $nom_paciente){
                            ///$arrIDspacientes[] = $id_paciente;
                            ///$arrNombres[] = $nombre;
                            ///$truckId = $arrTrucksIds[$truckIds];
                            ///$arrIDspacientes[$arrIndx];
                            echo '<option value="'.$arrIDspacientes[$arrIndx].'" >'.$nom_paciente.'</option>';
                            $arrIndx++;
                          
                          }
                          echo '</select>';
                          
                          if(count($arrNombres) == 0){
                            echo '<div class="alert alert-warning">';
                              echo '<p data-dismiss="alert">No hay pacientes en la base de datos. Favor de agregar al menos uno.</p>';
                            echo '</div>';
                          }

                        }else{
                          $cant_pacientes = 1;
                          echo '<input type="text" class="form-control col-sm-6" id="nombre_pac" name="nombre_pac"  value="'.$nombre.'" required  >';
                        
                        }
                      ?>                      
                    </div>

					<div class="col" id="divNompaciente" >
                      <label for="inputAddress" >Nombre</label >
                      <input type="text" class="form-control  col-sm-8" id="nom_paciente_sinexp" name="nom_paciente_sinexp" >                    
                   </div>					

					</div>


                    
					<div class="form-row">
					
						<div class="col">
						  <label for="inputAddress">Teléfono</label >
						  <input type="text" class="form-control  col-sm-8" id="tel_paciente" name="tel_paciente" required>                    
					   </div>
						<div class="col">
						  <label for="inputAddress">Fecha de la cita</label >
						  <input type="date" class="form-control  col-sm-8" id="fecha_cita" name="fecha_cita" required>                    
					   </div>
						<div class="col">
						  <label for="inputAddress">Hora de la cita</label >
						  <input type="text" class="form-control  col-sm-8" id="hora_cita" name="hora_cita" placeholder="hh:mm" required>                    
					   </div>
					</div>              
              </div>			

              <h3>Motivo visita</h3>
              <div class="form-row estilo1">

              <!-- Motivo de visita: id_motivo_visita -->
                <div class="col" >
					  <label for="inputAddress">Motivos de Visita</label >
						
						<?
						  $qry="SELECT * FROM `geadental_hcc_motivos_visita` WHERE isActive = 1";

						  try {
							$result = mysqli_query($link, $qry);

							echo '<select class="form-control col-sm-8" id="sd_motif_vis" name="sd_motif_vis" required>';
							  while($row = $result->fetch_assoc()){
								$id_motivo_visita = $row['id_motivo_visita'];
								$motivo_visita = $row['motivo_visita'];
						  
								if(trim($motivo_visita) != "")         
								  echo '<option value="'.$id_motivo_visita.'">'.$motivo_visita.'</option>';
							
								$rowCount ++;       
							  }

							echo '</select>';
							$rows = $rowCount;
							$cant_motivos = count($result);
							if($cant_motivos == 0){
							  echo '<div class="alert alert-warning">';
								echo '<p data-dismiss="alert">No hay motivos de visita en la base de datos. Favor de agregar al menos uno.</p>';
							  echo '</div>';
							}                        
						  }

						  //catch exception
						  catch(Exception $e) {
							echo 'Message: ' .$e->getMessage();
						  }

						?>
				</div>
				
                <!-- Motivo de visita: hechos, signos y sintomas -->
                  <div class="col">
                    <label for="inputAddress">Hechos</label >
                    <input type="text" class="form-control  col-sm-8" id="hechos_cita" name="hechos_cita" placeholder="Dolor de muela desde hace dias" required>                    
                 </div>	
				
			  </div>
			  <br><br>
				<button type="submit" class="btn btn-success" id="agregarcita" name="agregarcita"><i class="fa fa-check-square"></i> Agregar Cita</button>
			
            </form>
          </div>
        </div>
      </section>
      <!-- Fin de la página-->
      <!-- Modals -->
      <!-- Modals -->
      <!--==========================
      Footer
      ============================-->
      <footer class="footer">
        <div class="container">
        </div>
    <div class="copyrights">
      <div class="container">
        <!-- <p>&copy; Copyrights eStartup. All rights reserved.</p> -->
        <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
          -->
          <!-- Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a> Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
          Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a>
        </div>
      </div>
    </div>
      </footer>
      <!-- JavaScript Libraries -->
	  <script>
	  function HabilitaNombre(){
		  var listaPacientes = document.getElementById("nombre_pac_sel");
		  var nom_paciente_sinexp = document.getElementById("divNompaciente");
		  
		  if(listaPacientes == 1){
			  nom_paciente_sinexp.removeAttribute("hidden");
			  console.log("nombre: "+listaPacientes.value);
		  }
		  else{
			  nom_paciente_sinexp.setAttribute("hidden", true);
		  }
		 
		 
		  
	  }
	  </script>
	  
	  
      <script src="lib/jquery/jquery.min.js"></script>
      <script src="lib/jquery/jquery-migrate.min.js"></script>
      <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="lib/superfish/hoverIntent.js"></script>
      <script src="lib/superfish/superfish.min.js"></script>
      <script src="lib/easing/easing.min.js"></script>
      <script src="lib/modal-video/js/modal-video.js"></script>
      <script src="lib/owlcarousel/owl.carousel.min.js"></script>
      <script src="lib/wow/wow.min.js"></script>
      <!-- Contact Form JavaScript File -->
      <script src="contactform/contactform.js"></script>
      <!-- Template Main Javascript File -->
      <script src="js/main.js"></script>
    </body>
  </html>