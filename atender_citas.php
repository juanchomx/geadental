<?
  include("connection.php");
  include("gen_functions.php");
  session_start();

  if($_SESSION['loggedin'] == 0)
    die("<script>location.href = 'login.php'</script>");
  else{
    $iduser = $_SESSION['iduser'];
    $name = $_SESSION['name'];    
    $id_user_type = $_SESSION['id_user_type'];
    $idpanel = 3;///Pacientes

    ///cita
    (int)$id_cita = $_GET['id_cita'];

  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>GEA Dental</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">
    <!-- Bootstrap css -->
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Libraries CSS Files -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/modal-video/css/modal-video.min.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">
    <!-- =======================================================
    Theme Name: eStartup
    Theme URL: https://bootstrapmade.com/estartup-bootstrap-landing-page-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
    ======================================================= -->
  </head>
  <body>
    <header id="header" class="header header-hide">
      <div class="container">
        <div id="logo" class="pull-left">
          <!-- <h1><a href="#body" class="scrollto"><span>e</span>Startup</a></h1> -->
          <!-- Uncomment below if you prefer to use an image logo -->
          <a href="#body"><img src="img/logo.png" alt="GEA Dental" title="" height="40" width="40" /></a>
        </div>
        
        <?
          ///Desplegar menu
         $menu = obtenerMenu($link, $id_user_type, $idpanel);
         if($menu != "")
          echo $menu;          
        ?>

      </div>
      </header><!-- #header -->
      <!--==========================
      Features Section
      ============================-->
      <section id="features" class="padd-section text-center wow fadeInUp">
        <div class="container">
          <div class="section-title text-center">
            <h2>Atendiendo cita</h2>
            <br><br>

            <table class="table table-striped table-dark table-hover table-bordered">
              <thead>
                <tr>
                  <th scope="col">Num Cita</th>
                  <th scope="col">Hora</th>
                  <th scope="col">Paciente</th>
                  <th scope="col">Motivo de Visita</th>
                  <th scope="col">Hechos</th>
                </tr>
              </thead>
              <tbody>
                <?
                  $qry="
                      SELECT id_cita ,b.nombre as 'paciente' ,hora_cita ,c.motivo_visita as 'motif visita', hechos
                      FROM `geadental_citas` as a
                      JOIN geadental_hcc_motivos_visita as c
                      ON  a.id_motivo_visita = c.id_motivo_visita
                      JOIN geadental_hcc_info_personal as b
                      ON  a.id_paciente = b.id_paciente
                      WHERE a.fecha_cita = CURDATE() AND a.cita_atendida = 0
                  ";

                  try {  
                    $result = mysqli_query($link, $qry);
                    
                    $rowCount=0;
                    while($row = $result->fetch_assoc()){
                      (int)$id_cita = $row['id_cita'];
                      $paciente = $row['paciente'];
                      $hora_cita = $row['hora_cita'];
                      $motivo_visita = $row['motif visita'];
                      $hechos = $row['hechos'];

                      echo '<tr>';
                        echo '<th scope="row">'.$id_cita.'</th>';
                        ///echo "<th scope='row' class='col-md-1'><a href='loads_update.php?loadnum=" .$load_num. "'> ".$load_num.  '</a></th>';
                        echo '<td>'.$hora_cita.'</td>';
                        echo '<td>'.$paciente.'</td>';
                        echo '<td>'.$motivo_visita.'</td>';
                        echo '<td>'.$hechos.'</td>';
                        ///echo "<td> <button type='button' ><a href=atender_citas.php.php?id_cita=".$id_cita." target='_blank'>Atender Cita</a></button> </td>";
                        ////echo "<td> <button type='button' ><a href=rpts/invoicerpt.php?load_num=".$loadNum." target='_blank'>Quick Print</a></button> </td>";
                        ////echo ''; ////http://syner.info/geadental/agregar_citas.php?id_paciente=0
                      echo '</tr>';
                      
                      $rowCount ++;
                    }
                    
                    if($rowCount == 0){
                    echo '<div class="alert alert-info">';
                      echo '<p data-dismiss="alert">No hay citas para hoy</p>';
                    echo '</div>';
                    }
                  }  
                  catch(exception $e) {
                    ///echo "ex: ".$e; 
                    echo '<div class="alert alert-warning">';
                    echo '<p data-dismiss="alert">Error al tratar de las lista de citas: '.$msg.'. Favor de contactar al administrador</p>';
                    echo '</div>';
                    mysql_close($link);
                    die();
                  }

                ?>
              </tbody>
            </table>

            <!-- Formulario: agrgar pacientes -->
            <form method="post" action="dbcrud1.php">

              <!-- Paciente y Fecha -->
              <input type="text" class="form-control col-sm-6" id="id_cita" name="id_cita" value="<? echo $id_cita ?>" >
              <div id="ip_ID" class="estilo1" hidden>
                  <div class="form-row">
                    <div class="col">
                      <label for="inputAddress">Paciente</label ><br><!--&nbsp; &nbsp;<label id="msgs" class="warningmsg1" hidden></label><br> -->
                      <!-- <input type="text" class="form-control col-sm-6" id="nombre_pac" name="nombre_pac"  placeholder="nombre paciente" required  > -->
                      <?
                        if($desp_lista_pacientes){

                          echo '<select class="form-control col-sm-8" id="nombre_pac_sel" name="nombre_pac_sel">';
                          $arrIndx = 0;
                          foreach($arrNombres as $nom_paciente){
                            ///$arrIDspacientes[] = $id_paciente;
                            ///$arrNombres[] = $nombre;
                            ///$truckId = $arrTrucksIds[$truckIds];
                            ///$arrIDspacientes[$arrIndx];
                            echo '<option value="'.$arrIDspacientes[$arrIndx].'" >'.$nom_paciente.'</option>';
                            $arrIndx++;
                          
                          }
                          echo '</select>';
                          $cant_pacientes = count($arrIDspacientes);
                          if($cant_pacientes == 0){
                            echo '<div class="alert alert-warning">';
                              echo '<p data-dismiss="alert">No hay pacientes en la base de datos. Favor de agregar al menos uno.</p>';
                            echo '</div>';
                          }

                        }else{
                          $cant_pacientes = 1;
                          echo '<input type="text" class="form-control col-sm-6" id="nombre_pac" name="nombre_pac"  value="'.$nombre.'" required  >';
                        
                        }
                      ?>                      
                    </div>
                    <div class="col">
                      <label for="inputAddress">Fecha de la cita</label >
                      <input type="date" class="form-control  col-sm-8" id="fecha_cita" name="fecha_cita" required>                    
                   </div>
                    <div class="col">
                      <label for="inputAddress">Hora de la cita</label >
                      <input type="text" class="form-control  col-sm-8" id="hora_cita" name="hora_cita" placeholder="hh:mm" required>                    
                   </div>
                </div>              
              </div>

              <h3>Motivo visita</h3> 
              <div class="form-row estilo1" >

                <!-- Motivo de visita: Observaciones -->
                  <div class="col" >
                    <label for="inputAddress">Observaciones</label >
                    <input type="text" class="form-control  col-sm-8" id="observ_cita" name="observ_cita" placeholder="Obs médico" required>                    
                 </div>

              

              <!-- Motivo de visita: Signos -->
              
              <!-- Motivo de visita: TA -->
                <div class="col">
                  <label for="inputAddress">TA</label >
                  <input type="text" class="form-control  col-sm-8" id="ta_cita" name="ta_cita" value="120/80" required>                    
               </div>
              <!-- Motivo de visita: Glucosa -->
                <div class="col">
                  <label for="inputAddress">Glucosa</label >
                  <input type="text" class="form-control  col-sm-8" id="glucosa_cita" name="glucosa_cita" value="90" required>                    
               </div>
              </div>              

              <h3>Diagnóstico</h3>
              <!-- <h3>Diagnóstico</h3> -->
              <div class="form-row estilo1" >

                <!-- Diagnóstico: id_diagnostico -->
                  <div class="col" >
                    <label for="inputAddress">Diagnóstico</label >
                      
                      <?
                        unset($qry);
                        unset($row);
                        unset($result);
                        $qry="SELECT * FROM `geadental_diagnosticos` ORDER BY diagnostico ASC";

                        try {
                          $result = mysqli_query($link, $qry);

                          $rows = 0;
                          echo '<select class="form-control col-sm-8" id="dx_cita" name="dx_cita" required>';
                            while($row = $result->fetch_assoc()){
                              $id_diagnostico = $row['id_diagnostico'];
                              $diagnostico = $row['diagnostico'];
                        
                              if(trim($diagnostico) != "")         
                                echo '<option value="'.$id_diagnostico.'">'.$diagnostico.'</option>';
                          
                              $rowCount ++;       
                            }

                          echo '</select>';
                          $rows = $rowCount;
                          ///echo '<br>'.$qry.'<br>'.$rows.'<br>cant reg: '.count($row).'<br>';
                          if($rows == 0){
                            echo '<div class="alert alert-warning">';
                              echo '<p data-dismiss="alert">No hay diagnosticos en la base de datos. Favor de agregar al menos uno.</p>';
                            echo '</div>';
                          }                        
                        }

                        //catch exception
                        catch(Exception $e) {
                          echo 'Message: ' .$e->getMessage();
                        }

                      ?>
                  </div>

                  <!-- recomendaciones -->
                  <div class="col" >
                    <label for="inputAddress">Recomendaciones</label >
                    <input type="text" class="form-control  col-sm-10" id="reco_cita" name="reco_cita" placeholder="Recomendaciones del médico" required>
                  </div>
              </div>

             

              <br><br>
              <!-- <button type="submit" class="btn btn-success" id="agregarpciente" name="agregarpciente"><i class="fa fa-check-square"></i> Agregar Cita</button> -->
              <button type="submit" class="btn btn-success" id="regcita" name="regcita"><i class="fa fa-check-square"></i> Registrar Cita</button>

            </form>
          </div>
        </div>
      </section>
      <!-- Fin de la página-->
      <!-- Modals -->
      <!-- Modals -->
      <!--==========================
      Footer
      ============================-->
      <footer class="footer">
        <div class="container">
        </div>
    <div class="copyrights">
      <div class="container">
        <!-- <p>&copy; Copyrights eStartup. All rights reserved.</p> -->
        <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
          -->
          <!-- Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a> Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
          Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a>
        </div>
      </div>
    </div>
      </footer>
      <!-- JavaScript Libraries -->
      <script src="lib/jquery/jquery.min.js"></script>
      <script src="lib/jquery/jquery-migrate.min.js"></script>
      <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="lib/superfish/hoverIntent.js"></script>
      <script src="lib/superfish/superfish.min.js"></script>
      <script src="lib/easing/easing.min.js"></script>
      <script src="lib/modal-video/js/modal-video.js"></script>
      <script src="lib/owlcarousel/owl.carousel.min.js"></script>
      <script src="lib/wow/wow.min.js"></script>
      <!-- Contact Form JavaScript File -->
      <script src="contactform/contactform.js"></script>
      <!-- Template Main Javascript File -->
      <script src="js/main.js"></script>
    </body>
  </html>