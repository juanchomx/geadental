<?
  include("connection.php");
  include("gen_functions.php");
  session_start();

  if($_SESSION['loggedin'] == 0)
    die("<script>location.href = 'login.php'</script>");
  else{
    $iduser = $_SESSION['iduser'];
    $name = $_SESSION['name'];    
    $id_user_type = $_SESSION['id_user_type'];
    $idpanel = 2;///Pacientes
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>GEA Dental</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">
    <!-- Bootstrap css -->
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Libraries CSS Files -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/modal-video/css/modal-video.min.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">
    <!-- =======================================================
    Theme Name: eStartup
    Theme URL: https://bootstrapmade.com/estartup-bootstrap-landing-page-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
    ======================================================= -->
  </head>
  <body>
    <header id="header" class="header header-hide">
      <div class="container">
        <div id="logo" class="pull-left">
          <!-- <h1><a href="#body" class="scrollto"><span>e</span>Startup</a></h1> -->
          <!-- Uncomment below if you prefer to use an image logo -->
          <a href="#body"><img src="img/logo.png" alt="GEA Dental" title="" height="40" width="40" /></a>
        </div>
        
        <?
          /*
          <nav id="nav-menu-container">
            <ul class="nav-menu">
              <li ><a href="index.php">Inicio</a></li>
              <li class="menu-active"> <a href="pacientes.php">Pacientes</a></li>
              <!--
              <li><a href="#features">Features</a></li>
              <li><a href="#screenshots">Screenshots</a></li>
              <li><a href="#team">Team</a></li>
              <li><a href="#pricing">Pricing</a></li>
              -->
              <li class="menu-has-children"><a href="">Conta</a>
              <ul>
                <li><a href="#">Inventario</a></li>
                <li><a href="#">Balance</a></li>
              </ul>
            </li>
            <!--
            <li><a href="#blog">Blog</a></li>
            <li><a href="#contact">Contact</a></li>
            -->
            <li><a href="login.php?logout=1" class="external"><span class="glyphicon glyphicon-log-out"></span> Salir</a></li>
          </ul>
          </nav><!-- #nav-menu-container -->
          */
          ///Desplegar menu
         $menu = obtenerMenu($link, $id_user_type, $idpanel);
         if($menu != "")
          echo $menu;          
        ?>

      </div>
      </header><!-- #header -->
      <!--==========================
      Features Section
      ============================-->
      <section id="features" class="padd-section text-center wow fadeInUp">
        <div class="container">
          <div class="section-title text-center">
            <h2>Agregar Paciente</h2>
            <br><br>
            <!-- Formulario: agrgar pacientes -->
            <form method="post" action="dbcrud1.php">

              <!-- Info personal: ID -->
              <h3>Información Personal</h3>
              <div id="ip_ID" class="estilo1">
                  <div class="form-row">
                    <div class="col">
                      <label for="inputAddress">Nombre</label ><!--&nbsp; &nbsp;<label id="msgs" class="warningmsg1" hidden></label><br> -->
                      <input type="text" class="form-control col-sm-6" id="nombre_pac" name="nombre_pac"  placeholder="nombre paciente" required  >
                    </div>
                    <div class="col">
                      <label for="inputAddress">Sexo</label >
                        <select class="form-control col-sm-6" id="sexo_pac" name="sexo_pac" onchange=" mostrarsecfem()">
                          <option value="2">Masculino</option>
                          <option value="1">Femenino</option>
                        </select>                    
                   </div>
                    <div class="col">
                      <label for="inputAddress">Fecha Nacimiento</label >
                      <input type="date" class="form-control  col-sm-8" id="paciente_fnac" name="paciente_fnac" required>  
                   </div>                   
                </div>              
              </div>

              <!-- Info personal: Contacto -->
              <div id="ip_Contacto" class="estilo1">                
                  <div class="form-row">
                    <div class="col">
                      <label for="inputAddress">Dirección</label >
                      <input type="text" class="form-control col-sm-8" id="dir_pac" name="dir_pac" placeholder="Av. Coatzacoalcos #4221" required  >
                    </div>
                   
                    <div class="col">
                      <label for="inputAddress">Colonia</label >
                      <input type="text" class="form-control col-sm-8" id="dir_col_pac" name="dir_col_pac" placeholder="Felipe Angeles" required  >
                   </div>

                    <div class="col">
                      <label for="inputAddress">Tel. Contacto 1</label >
                      <input type="text" class="form-control  col-sm-8" id="tel1_pac" name="tel1_pac" placeholder="0123456789" required>  
                   </div>



                  </div>
                
                  <div class="form-row">
                    <div class="col">
                      <label for="inputAddress">Correo Electrónico</label >
                      <input type="email" class="form-control  col-sm-8" id="email_pac" name="email_pac" placeholder="juan@mail.com" required>  
                   </div>

                    <div class="col">
                      <label for="inputAddress">Tel. Contacto 2</label >
                      <input type="text" class="form-control  col-sm-8" id="tel2_pac" name="tel2_pac" placeholder="0123456789" required>  
                   </div>                    
                  </div>

                  <div class="form-row">

                    <div class="col">
                      <label for="inputAddress">Nombre Contacto Emergencia</label >
                      <input type="text" class="form-control col-sm-8" id="dir_pac" name="nom_cont_em_pac" placeholder="Contacto de emergencia" required  >
                    </div>
                    <div class="col">
                      <label for="inputAddress">Te. Contacto Emergencia</label >
                      <input type="text" class="form-control col-sm-8" id="dir_col_pac" name="tel_cont_em_pac" placeholder="0123456789" required  >
                   </div>
                    <div class="col">
                      <label for="inputAddress">Nombre Quién recomienda</label >
                      <input type="text" class="form-control  col-sm-8" id="tel1_pac" name="quien_recomienda_pac" placeholder="Contacto de emergencia" required>  
                   </div>

                  </div>

              </div>
              <!-- Info Personal: Estado, civil, hijos, quien recomienda -->
              <div id="ip_civil_hijos" class="estilo1">

                  <div class="form-row">

                    <div class="col">
                      <label for="inputAddress">Estado Civil</label >
                        <select class="form-control col-sm-6" id="civil_pac" name="civil_pac" >
                          <option value="1">Soltero(a)</option>
                          <option value="2">Casado(a)</option>
                          <option value="3">Divorciado(a)</option>
                          <option value="4">Viudo(a)</option>
                          <option value="5">Unión Libre</option>
                        </select>
                    </div>

                    <div class="col">
                      <label for="inputAddress">Cant. Hijos</label >
                      <input type="numeric" class="form-control  col-sm-2" id="canthijos_pac" name="canthijos_pac" required>  
                   </div>                    

                    <div class="col">
                      <label for="inputAddress">Ocupación</label >
                      <input type="text" class="form-control  col-sm-8" id="ocupacion_pac" name="ocupacion_pac" placeholder="Profesionista" required>  
                   </div>

                </div>
              
              </div>

              <h3>Salud General</h3>
              <div class="form-row estilo1">

                <!-- Salud general: ataques_cardiacos -->
                <div class="col">
                  <label for="inputAddress">Ataques Cardiacos</label >
                    <select class="form-control col-sm-4" id="sg_card_at" name="sg_card_at" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>
              <!-- Salud general: dolor_de_pecho -->
                <div class="col">
                  <label for="inputAddress">Dolor de Pecho</label >
                    <select class="form-control col-sm-4" id="sg_card_dolorpecho" name="sg_card_dolorpecho" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>
              <!-- Salud general: tiene_marcapasos -->
                <div class="col">
                  <label for="inputAddress">Tiene Marcapasos</label >
                    <select class="form-control col-sm-4" id="sg_card_marcapasos" name="sg_card_marcapasos" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>
              <!-- Salud general: presion_alta -->
                <div class="col">
                  <label for="inputAddress">Presión Alta</label >
                    <select class="form-control col-sm-4" id="sg_card_pr_alta" name="sg_card_pr_alta" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>              
              <!-- Salud general: presion_baja -->
                <div class="col">
                  <label for="inputAddress">Presión Baja</label >
                    <select class="form-control col-sm-4" id="sg_card_pr_baja" name="sg_card_pr_baja" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>

              </div>
              
              <div class="form-row estilo1">
              <!-- Salud general: falta_de_aire -->
                <div class="col">
                  <label for="inputAddress">Falta de Aire</label >
                    <select class="form-control col-sm-4" id="sg_falta_aire" name="sg_falta_aire" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>              
              <!-- Salud general: asma -->
                <div class="col">
                  <label for="inputAddress">Asma</label >
                    <select class="form-control col-sm-4" id="sg_asma" name="sg_asma" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>
              <!-- Salud general: diabetes -->
                <div class="col">
                  <label for="inputAddress">Diabetes</label >
                    <select class="form-control col-sm-4" id="sg_diabetes" name="sg_diabetes" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>              
              <!-- Salud general: zumbido_de_oidos -->
                <div class="col">
                  <label for="inputAddress">Zumbido de oidos</label >
                    <select class="form-control col-sm-4" id="sg_zumb_oidos" name="sg_zumb_oidos" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>
              <!-- Salud general: hepatitis -->
                <div class="col">
                  <label for="inputAddress">Hepatitis</label >
                    <select class="form-control col-sm-4" id="sg_hepatitis" name="sg_hepatitis" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>

              </div>

              <div class="form-row estilo1">
              <!-- Salud general: tuberculosis -->
                <div class="col">
                  <label for="inputAddress">Tuberculosis</label >
                    <select class="form-control col-sm-4" id="sg_tuberculosis" name="sg_tuberculosis" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>
              <!-- Salud general: epilepsia -->
                <div class="col">
                  <label for="inputAddress">Epilepsia</label >
                    <select class="form-control col-sm-4" id="sg_epilepsia" name="sg_epilepsia" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>
              <!-- Salud general: ulceras -->
                <div class="col">
                  <label for="inputAddress">Ulceras</label >
                    <select class="form-control col-sm-4" id="sg_ulceras" name="sg_ulceras" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>              
              <!-- Salud general: anemia -->
                <div class="col">
                  <label for="inputAddress">Anemia</label >
                    <select class="form-control col-sm-4" id="sg_anemia" name="sg_anemia" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>               
              <!-- Salud general: tumores_o_cancer -->
                <div class="col">
                  <label for="inputAddress">Tumores o Cáncer</label >
                    <select class="form-control col-sm-4" id="sg_tumores_cancer" name="sg_tumores_cancer" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>              


              </div>

              <div class="form-row estilo1">
              <!-- Salud general: osteoporosis -->
                <div class="col">
                  <label for="inputAddress">Osteoporósis</label >
                    <select class="form-control col-sm-4" id="sg_osteoporosis" name="sg_osteoporosis" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>

              <!-- Salud general: enf_venereas -->
                <div class="col">
                  <label for="inputAddress">Enfermedades Venéreas</label >
                    <select class="form-control col-sm-4" id="sg_enf_venereas" name="sg_enf_venereas" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>              
              <!-- Salud general: expuesto_a_SIDA -->
                <div class="col">
                  <label for="inputAddress">Expuesto a SIDA</label >
                    <select class="form-control col-sm-4" id="sg_exp_sida" name="sg_exp_sida" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>              
              </div>
              
              <!-- Salud general: Mujer -->
              <div id="ip_mujer" class="estilo1" hidden>
                <h3>Información Personal Paciente Femenino</h3>
              </div>

              <h3>Salud Dental</h3>
              <div class="form-row estilo1">
              <!-- id_motivo_visita -->
                <div class="col" hidden>
                  <label for="inputAddress">Motivos de Visita</label >
                    
                    <?
                      $qry="SELECT * FROM `geadental_hcc_motivos_visita` WHERE isActive = 1";
                      


                      try {
                        $result = mysqli_query($link, $qry);

                        echo '<select class="form-control col-sm-6" id="sd_motif_vis" name="sd_motif_vis" required>';
                          while($row = $result->fetch_assoc()){
                            $id_motivo_visita = $row['id_motivo_visita'];
                            $motivo_visita = $row['motivo_visita'];
                      
                            if(trim($motivo_visita) != "")         
                              echo '<option value="'.$id_motivo_visita.'">'.$motivo_visita.'</option>';
                        
                            $rowCount ++;       
                          }

                        echo '</select>';
                        $rows = $rowCount;

                        if($rows == 0){
                          echo '<div class="alert alert-warning">';
                            echo '<p data-dismiss="alert">No hay motivos de visita en la base de datos. Favor de agregar al menos uno.</p>';
                          echo '</div>';
                        }                        
                      }

                      //catch exception
                      catch(Exception $e) {
                        echo 'Message: ' .$e->getMessage();
                      }

                    ?>
                </div>

              <!-- visitado_otro_dentista -->
                <div class="col" hidden>
                  <label for="inputAddress">¿Ha visitado otro dentista?</label >
                    <select class="form-control col-sm-4" id="sd_visitado_otro_dentista" name="sd_enf_visitado_otro_dentista" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>

              <!-- nom_otro_dentista -->
                <div class="col" hidden>
                  <label for="inputAddress">Nombre Otro Dentista</label >
                  <input type="text" class="form-control  col-sm-8" id="sd_nom_otro_dent" name="sd_nom_otro_dent" value="N/A" placeholder="Profesionista" required>  
               </div>

              </div>

              <div class="form-row estilo1">
              <!-- sangran_encias_cepillarse -->
                <div class="col">
                  <label for="inputAddress">¿Sangran encias al cepillarse?</label >
                    <select class="form-control col-sm-4" id="sd_sangran_encias" name="sd_sangran_encias" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>
              <!-- limpieza_dental (está agregado como motivo de consulta-->
              
              <!-- dolor_cambios_temp -->
                <div class="col">
                  <label for="inputAddress">¿Dolor/Cambios de temp?</label >
                    <select class="form-control col-sm-4" id="sd_cambios_temp" name="sd_cambios_temp" >
                      <option value="0">No</option>
                      <option value="1">Dolor</option>
                      <option value="1">Cambio Temp</option>
                    </select>
                </div>

                <!-- hinchazon_boca -->
                <div class="col">
                  <label for="inputAddress">¿Hinchazón de boca?</label >
                    <select class="form-control col-sm-4" id="sd_hinch_boca" name="sd_hinch_boca" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>

              </div>

              <div class="form-row estilo1">
                <!-- anestesia -->
                <div class="col">
                  <label for="inputAddress">¿Anestesia?</label >
                    <select class="form-control col-sm-4" id="sd_anestesia" name="sd_anestesia" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>

              <!-- alergico_a_medicamento -->
                <div class="col">
                  <label for="inputAddress">¿Alergico a algún medicamento?</label >
                    <select class="form-control col-sm-4" id="sd_alergico" name="sd_alergico" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>

              <!-- medicamento_alergico -->
                <div class="col">
                  <label for="inputAddress">Medicamento alergia</label >
                  <input type="text" class="form-control  col-sm-8" id="sd_med_alergia" name="sd_med_alergia" value="N/A" placeholder="ej penicilina" required>
                </div>

              </div>
              
              <div class="form-row estilo1">
              <!-- dolores_de_cabeza -->
                <div class="col">
                  <label for="inputAddress">¿Dolores de cabeza?</label >
                    <select class="form-control col-sm-4" id="sd_dol_cabeza" name="sd_dol_cabeza" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>

              <!-- cuello -->
                <div class="col">
                  <label for="inputAddress">¿Dolores de cuello?</label >
                    <select class="form-control col-sm-4" id="sd_dol_cuello" name="sd_dol_cuello" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div> 

              <!-- dolor_en_los_oidos -->
                <div class="col">
                  <label for="inputAddress">¿Dolor en los oidos?</label >
                    <select class="form-control col-sm-4" id="sd_dol_oidos" name="sd_dol_oidos" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>

              <!-- mareos -->
                <div class="col">
                  <label for="inputAddress">¿Mareos?</label >
                    <select class="form-control col-sm-4" id="sd_mareos" name="sd_mareos" >
                      <option value="0">No</option>
                      <option value="1">Si</option>
                    </select>
                </div>

            </div>              

              <br><br>
              <button type="submit" class="btn btn-success" id="agregarpciente" name="agregarpciente"><i class="fa fa-check-square"></i> Agregar Paciente</button>
            </form>
          </div>
        </div>
      </section>
      <!-- Fin de la página-->
      <!-- Modals -->
      <!-- Modals -->
      <!--==========================
      Footer
      ============================-->
      <footer class="footer">
        <div class="container">
        </div>
        <div class="copyrights">
          <div class="container">
            <p>&copy; Copyrights eStartup. All rights reserved.</p>
            <div class="credits">
              <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
              -->
              Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a> Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
          </div>
        </div>
      </footer>
      <!-- JavaScript Libraries -->
      <script src="lib/jquery/jquery.min.js"></script>
      <script src="lib/jquery/jquery-migrate.min.js"></script>
      <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="lib/superfish/hoverIntent.js"></script>
      <script src="lib/superfish/superfish.min.js"></script>
      <script src="lib/easing/easing.min.js"></script>
      <script src="lib/modal-video/js/modal-video.js"></script>
      <script src="lib/owlcarousel/owl.carousel.min.js"></script>
      <script src="lib/wow/wow.min.js"></script>
      <!-- Contact Form JavaScript File -->
      <script src="contactform/contactform.js"></script>
      <!-- Template Main Javascript File -->
      <script src="js/main.js"></script>
    </body>
  </html>