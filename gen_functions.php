<?
	include("connection.php");

	/*
	//////////////////////////////
	params: link, nombre, f nacim, telefono
	returns: id de paciente
	autor: JM
	fecha: Dic 2019
	//////////////////////////////
	*/
	function obtenerMenu($link, $idusertype, $idpanel){
		$menu="";
		try {  
			$qry="SELECT menu FROM `geadental_app_menus` WHERE id_user_type = ".$idusertype." AND id_panel = ".$idpanel." AND isActive = 1 LIMIT 1";
			
			$result1 = mysqli_query($link, $qry);
			$row = $result1->fetch_assoc();
			$menu = $row['menu'];
			
			///echo '<br>qry: '.$qry.'<br>id_paciente: '.$id_paciente.'<br>';
			
		}  
		catch(exception $e) {
		  ///echo "ex: ".$e; 
		  $msg='Message: ' .$e->getMessage();
		  
		  echo '<div class="alert alert-warning">';
			echo '<p data-dismiss="alert">Error al tratar el menu: '.$msg.'. Favor de contactar al administrador</p>';
			echo '<button><a href="index.php" ></a>Regresar</button>';
		  echo '</div>';
		  mysql_close($link);
		  die();
		  
		}
		
		return $menu;
	}

			/*
	//////////////////////////////
	params: link, qry
	returns: resultados de un query
	autor: JM
	fecha: Dic 2019
	//////////////////////////////
	*/
	function obtenerMultivalues($link, $qry){
		$resp="";
		try {  			
			
			$result1 = mysqli_query($link, $qry);
			$row = $result1->fetch_assoc();
			$resp = $row;
			
			///echo '<br>qry: '.$qry.'<br>id_paciente: '.$id_paciente.'<br>';
			
		}  
		catch(exception $e) {
		  ///echo "ex: ".$e; 
		  $msg='Message: ' .$e->getMessage();
		  
		  echo '<div class="alert alert-warning">';
			echo '<p data-dismiss="alert">Error al tratar el menu: '.$msg.'. Favor de contactar al administrador</p>';
			echo '<button><a href="index.php" ></a>Regresar</button>';
		  echo '</div>';
		  mysql_close($link);
		  die();
		  
		}
		
		return $resp;
	}
?>