<?
	include("dbcrud1.php");
	session_destroy(); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>GEA Dental</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">

  <!-- Bootstrap css -->
  <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/modal-video/css/modal-video.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: eStartup
    Theme URL: https://bootstrapmade.com/estartup-bootstrap-landing-page-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <header id="header" class="header header-hide">
    <div class="container">

      <div id="logo" class="pull-left">
        <!-- <h1><a href="#body" class="scrollto">GEA Dental</a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="#body"><img src="img/logo.png" alt="GEA Dental" title="" height="100" width="100" /></a>
      </div>


    </div>
  </header><!-- #header -->

  <!--==========================
    Hero Section
  ============================-->
  <section id="hero" class="wow fadeIn">
    <div class="hero-container">

	<div  align="center">	
		<h1>GEA Dental</h1>
		<h2>EXCELENCIA BUCAL</h2>
		
		<form method="post" action="dbcrud1.php">
			<label for="inputAddress">Usuario</label>&nbsp; 
			<input type="text" class="form-control col-sm-10" name="name" id="name" required>
			<br>
			<label for="inputAddress">Contraseña</label>&nbsp; 
			<input type="password" class="form-control  col-sm-10" name="pwd" id="pwd" required>
			<br>
			<input type="submit" value="Acceso" id="userlogin" name="userlogin" class="btn btn-success btn"  />
		</form>
	</div>
	  
      <!-- <img src="img/logo.png" alt="GEA Dental">-->
    </div>
  </section><!-- #hero -->

  <!--==========================
    Get Started Section
  ============================-->
  
  <!--==========================
    About Us Section
  ============================-->
  
  <!--==========================
    Features Section
  ============================-->
  
  <!--==========================
    Screenshots Section
  ============================-->
  
  <!--==========================
    Video Section
  ============================-->
  
  <!--==========================
    Team Section
  ============================-->
  
  <!--==========================
    Testimonials Section
  ============================-->
  
  <!--==========================
    Pricing Table Section
  ============================-->
  
  <!--==========================
    Blog Section
  ============================-->
  
  <!--==========================
    Newsletter Section
  ============================-->
  
  <!--==========================
    Contact Section
  ============================-->
  
  <!--==========================
    Footer
  ============================-->
  <footer class="footer">
  
    <div class="copyrights">
      <div class="container">
        <!-- <p>&copy; Copyrights eStartup. All rights reserved.</p> -->
        <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
          -->
          <!-- Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a> Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
          Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a>
        </div>
      </div>
    </div>

  </footer>



  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/modal-video/js/modal-video.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
