<?
	session_start();
	$unSec = 1000000;
	
	///Using pws hashin'
	$options = [
		'cost' => 12,
	];

	if($_GET["logout"]==1 AND $_SESSION['id']) {session_destroy();
		$message = "You've been logged out, have a nice day";
	}

	include("connection.php");

	//DB connection
	if( mysqli_connect_error()){
		echo mysqli_connect_error();
		die;//die "Could not connect to server";//mysqli_connect_error();
	}else{
	///Login
	if(isset($_POST['userlogin'])){
		session_destroy();
		$Name = htmlspecialchars($_POST['name']);
		$Pwd = htmlspecialchars($_POST['pwd']);
		
		$qryLogin = "SELECT * FROM `geadental_system_users` WHERE name = '".$Name."' AND password = '".$Pwd."'";
		$result1 = mysqli_query($link, $qryLogin);
		$row = $result1->fetch_assoc();
				
		if(count($row) > 0){
			////Verify if password has expired so the user changes it
			if($row['1s_time_pwd_changed'] == 0){
				session_start();
				$_SESSION['iduser'] = $row['iduser'];
				
				die("<script>location.href = 'change_password.php'</script>");
			}
			else{
				session_start();
				$_SESSION['iduser'] = $row['iduser'];
				$_SESSION['name'] = $row['name'];
				$_SESSION['id_user_type'] = $row['id_user_type'];
				$_SESSION['loggedin'] = 1;
				die("<script>location.href = 'index.php'</script>");
			}
		}else{
			echo '<img src="img/logo.jpg" alt="Profile pic" /><br><br>';
			//<button type="button" data-toggle="modal" data-target="#myModalAddContact" class="btn btn-primary" onclick="clearMsgs()">Add a contact</button>
			echo '<a href="login.php"><button type="button" class="btn btn-primary">Back</button></a>';
			echo '<br><br><h2>Incorrect user id or password.</h2>';			
		}
		
	}
	//Create Account
	if(isset($_POST['createacct'])){
		///Get values
		session_destroy();
		$Name = htmlspecialchars(trim($_POST['name']));
		$Pwd = htmlspecialchars(trim($_POST['newpwd1']));

		/*
		//Encrypt name
		$dbName = aes_encrypt($Name, hashhed($Name, $options));
		$d3crypt = aes_decrypt($dbName, hashhed($Name, $options));
		echo '<br>dbName AESdecrypted: '.$d3crypt;
		
		//Encrypt pwd
		$dbPwd = aes_encrypt($Pwd, hashhed($Pwd, $options));
		$d3crypt = aes_decrypt($dbPwd, hashhed($Pwd, $options));
		echo '<br>dbPwd AESdecrypted: '.$d3crypt.'<br>';
		
		$tbl = 'geadental_system_users';
		$fields = 'name, password, id_user_type, 1s_time_pwd_changed, isActive';
		$values = "'".$dbName."', '".$dbPwd."', 1, 1, 1";
		qryInsert($link, $tbl, $fields, $values);
		*/
		
		///$hashedPwd = password_hash($Pwd, PASSWORD_BCRYPT, $options);	
		///$hashedPwd = hashhed($Pwd, $options);
		///echo '<br>hashedPwd: '.$hashedPwd;
		$hashedName =  hashhed($Name, $options);
		///AES
		$enCrrpdbName = aes_encrypt($Name, $hashedName);
		///$encrypted = aes_encrypt($Pwd, $hashedPwd); // this yields a binary string
		///echo '<br>AESncrypted: '.$encrypted;
		
		$denCrrpdbName = aes_decrypt($enCrrpdbName, $hashedName);
		///$d3crypt = aes_decrypt($encrypted, $hashedPwd);
		
		echo '<br>AESdecrypted: '.$denCrrpdbName.'<br>';		
		
		$tbl = 'geadental_system_users';
		$fields = 'name, password, id_user_type, 1s_time_pwd_changed, isActive';
		$values = "'".$enCrrpdbName."', '".$Pwd."', 1, 1, 1";
		qryInsert($link, $tbl, $fields, $values);		
		
		//pswd very
		/*
		echo '<br>password_verify(): ';
		if (password_verify($Pwd, $hashedPwd)){
			//decrypted
			//echo '<br>crypted un-hash: '.decrypt($Pwd, $hash).'<br>';
			echo $d3crypt;
		}
		else 
			echo 'Invalid password.';	
		*/
		
		die();
		

		
		///die("<script>location.href = 'main.php'</script>");
	}
	
	///Login password change
	if(isset($_POST['pwdchange'])){
		session_start();
		$userId = $_SESSION['iduser'];
		$oldpwd =  htmlspecialchars(trim($_POST['oldpwd']));
		$pwd1 = htmlspecialchars(trim($_POST['newpwd1']));
		$pwd2 = htmlspecialchars(trim($_POST['newpwd2']));
		
		///Verify the old password is correct
		$qryCheckPwd = "SELECT * FROM `geadental_system_users` WHERE iduser = ".$userId." AND password = '".$oldpwd."'";
		$result1 = mysqli_query($link, $qryCheckPwd);
		$row = $result1->fetch_assoc();
		
		if(count($row) > 0){
			///Old password is correct, changing it to the new one.
			if($pwd1 == $pwd2){
				$newPwd = $pwd1;
				$qryUpdatepassword = "UPDATE `geadental_system_users` SET password = '".$newPwd."', 1s_time_pwd_changed = 1 WHERE iduser = ".$userId;
				mysqli_query($link, $qryUpdatepassword);

				$_SESSION['iduser'] = $row['iduser'];
				$_SESSION['name'] = $row['name'];
				$_SESSION['id_user_type'] = $row['id_user_type'];
				$_SESSION['loggedin'] = 1;
				
				///echo $qryCheckPwd;
				///die();
				die("<script>location.href = 'index.php'</script>");
				
			}else{
				echo '<img src="img/logo.png" alt="Profile pic" /><br><br>';
				//<button type="button" data-toggle="modal" data-target="#myModalAddContact" class="btn btn-primary" onclick="clearMsgs()">Add a contact</button>
				echo '<a href="change_password.php"><button type="button" class="btn btn-primary">Back</button></a>';
				echo '<br><br><h2>Old or new Passwords does not match.</h2>';				
			}
		}
		else{
			echo '<img src="img/logo.png" alt="Profile pic" /><br><br>';
			//<button type="button" data-toggle="modal" data-target="#myModalAddContact" class="btn btn-primary" onclick="clearMsgs()">Add a contact</button>
			echo '<a href="change_password.php"><button type="button" class="btn btn-primary">Back</button></a>';
			echo '<br><br><h2>Old pssword does not match.</h2>';			
		}		
	}
	///agregarpciente
	if(isset($_POST['agregarpciente'])){
		
		//// Datos del formulario ////
		////Info general
		$nombre_pac = htmlspecialchars(trim($_POST['nombre_pac']));
		$sexo_pac = $_POST['sexo_pac']; ///htmlspecialchars($_POST['nombre_pac']);

		$rawdate1 = htmlentities($_POST['paciente_fnac']);
		$paciente_fnac = date('Y-m-d', strtotime($rawdate1));

		$dir_pac = htmlspecialchars(trim($_POST['dir_pac']));///calle
		$dirnum_pac = htmlspecialchars(trim($_POST['dirnum_pac']));///numero
		$dir_col_pac = htmlspecialchars(trim($_POST['dir_col_pac']));
		
		$tel1_pac = htmlspecialchars(trim($_POST['tel1_pac']));	
		if($tel1_pac == '')
			$tel1_pac = 0000;
		
		$email_pac = htmlspecialchars(trim($_POST['email_pac']));
		$rrss_pac = htmlspecialchars(trim($_POST['rrss_pac']));///sustituido por rrss
		$nom_cont_em_pac = htmlspecialchars(trim($_POST['nom_cont_em_pac']));
		$tel_cont_em_pac = htmlspecialchars(trim($_POST['tel_cont_em_pac']));
		$quien_recomienda_pac = htmlspecialchars(trim($_POST['quien_recomienda_pac']));
		$civil_pac = $_POST['civil_pac'];
		(int)$canthijos_pac = htmlspecialchars(trim($_POST['canthijos_pac']));
		$ocupacion_pac = htmlspecialchars(trim($_POST['ocupacion_pac']));

		/// Salud general
		/// Salud fem (si aplica)
		$rawdate1 = htmlentities($_POST['fecha_ultima_menstruacion']);
		$fecha_ultima_menstruacion = date('Y-m-d', strtotime($rawdate1));

		$embarazada = $_POST['embarazada'];
		$lactando = $_POST['lactando'];
		$problemas_menstruales = $_POST['problemas_menstruales'];

		unset($rawdate1);
		$rawdate1 = htmlentities($_POST['fecha_ultimo_parto']);
		$fecha_ultimo_parto = date('Y-m-d', strtotime($rawdate1));
		
		/// Gen
		$sg_card_at = $_POST['sg_card_at'];
		$sg_card_dolorpecho = $_POST['sg_card_dolorpecho'];
		$sg_card_marcapasos = $_POST['sg_card_marcapasos'];
		$sg_card_pr_alta = $_POST['sg_card_pr_alta'];
		$sg_card_pr_baja = $_POST['sg_card_pr_baja'];
		$sg_falta_aire = $_POST['sg_falta_aire'];
		$sg_asma = $_POST['sg_asma'];
		$sg_diabetes = $_POST['sg_diabetes'];
		$sg_zumb_oidos = $_POST['sg_zumb_oidos'];		
		$sg_hepatitis = $_POST['sg_hepatitis'];
		$sg_tuberculosis = $_POST['sg_tuberculosis'];
		$sg_epilepsia = $_POST['sg_epilepsia'];
		$sg_ulceras = $_POST['sg_ulceras'];
		$sg_anemia = $_POST['sg_anemia'];
		$sg_tumores_cancer = $_POST['sg_tumores_cancer'];
		$sg_osteoporosis = $_POST['sg_osteoporosis'];
		$sg_enf_venereas = $_POST['sg_enf_venereas'];
		$sg_exp_sida = $_POST['sg_exp_sida'];

		/// Salud dental
		$sd_motif_vis = 0;
		$sd_enf_visitado_otro_dentista = $_POST['sd_enf_visitado_otro_dentista'];
		$sd_nom_otro_dent = htmlspecialchars(trim($_POST['sd_nom_otro_dent']));
		$sd_sangran_encias = $_POST['sd_sangran_encias'];
		$sd_cambios_temp = $_POST['sd_cambios_temp'];
		$sd_hinch_boca = $_POST['sd_hinch_boca'];
		$sd_anestesia = $_POST['sd_anestesia'];
		$sd_alergico = $_POST['sd_alergico'];
		$sd_med_alergia = htmlspecialchars(trim($_POST['sd_med_alergia']));
		$sd_dol_cabeza = $_POST['sd_dol_cabeza'];
		$sd_dol_cuello = $_POST['sd_dol_cuello'];
		$sd_dol_oidos = $_POST['sd_dol_oidos'];
		$sd_mareos = $_POST['sd_mareos'];

		/// Signos
		$signos_ta = '';
		$signos_glucosa = '';

		///////////////////////////////////////
		/// Insertar registro de info general

		$qry="INSERT INTO `geadental_hcc_info_personal` (nombre,sexo,fecha_de_nacimiento,calle,numero,colonia,num_de_hijos,ocupacion,id_estado_civil,quien_lo_recomienda,tel1,correo_electronico,rrss,nom_contacto_emergencia,tel_contacto_emergencia,isActive) VALUES(
		'".$nombre_pac."',".$sexo_pac.",'".$paciente_fnac."','".$dir_pac."',".$dirnum_pac.",'".$dir_col_pac."',".$canthijos_pac.",'".$ocupacion_pac."',".$civil_pac.",'".$quien_recomienda_pac."','".$tel1_pac."','".$email_pac."','".$rrss_pac."','".$nom_cont_em_pac."','".$tel_cont_em_pac."',1)";
		
		///echo '<br>'.$qry.'<br>';
		
		exeQuery($link, $qry);
		
		unset($qry);
		usleep($unSec);
		(int)$id_paciente = obtenerIdpaciente($link, $nombre_pac, $paciente_fnac, $tel1_pac);
				
		///echo '<br><h3>id_paciente: </h3>'.$id_paciente.'<br>';
		
		if($id_paciente > 0){
			/// Agregar datos de salud mujer
			if($sexo_pac == 1){
				unset($qry);
				$qry="INSERT INTO `geadental_hcc_mujeres` (id_paciente,fecha_ultima_menstruacion,embarazada,lactando,problemas_menstruales,fecha_ultimo_parto) VALUES(".$id_paciente.",'".$fecha_ultima_menstruacion."',".$embarazada.",".$lactando.",".$problemas_menstruales.",'".$fecha_ultimo_parto."')";
				
				exeQuery($link, $qry);
				///echo '<br>'.$qry.'<br>';
			}
			
			/// Agregar datos de salud general
			$qry="INSERT INTO `geadental_hcc_salud_general` (				id_paciente,ataques_cardiacos,dolor_de_pecho,falta_de_aire,tiene_marcapasos,presion_alta,presion_baja,asma,diabetes,zumbido_de_oidos,hepatitis,tuberculosis,epilepsia,ulceras,anemia,tumores_o_cancer,osteoporosis,enf_venereas,expuesto_a_SIDA			
			) VALUES(
			".$id_paciente.",".$sg_card_at.",".$sg_card_dolorpecho.",".$sg_card_marcapasos.",".$sg_card_pr_alta.",".$sg_card_pr_baja.",".$sg_falta_aire.",".$sg_asma.",".$sg_diabetes.",".$sg_zumb_oidos.",".$sg_hepatitis.",".$sg_tuberculosis.",".$sg_epilepsia.",".$sg_ulceras.",".$sg_anemia.",".$sg_tumores_cancer.",".$sg_osteoporosis.",".$sg_enf_venereas.",".$sg_exp_sida.")";
		
			exeQuery($link, $qry);
			///echo '<br>'.$qry.'<br>';
			
			/// Agregar datos de salud dental
			unset($qry);
			usleep($unSec);			
			$qry="INSERT INTO `geadental_hcc_salud_dental` (		id_paciente,id_motivo_visita,visitado_otro_dentista,	 
				nom_otro_dentista,sangran_encias_cepillarse, 
				dolor_cambios_temp,hinchazon_boca,anestesia,alergico_a_medicamento,	 
				medicamento_alergico,dolores_de_cabeza,cuello,dolor_en_los_oidos,
				mareos		
			) VALUES(
			".$id_paciente.",".$sd_motif_vis.",".$sd_enf_visitado_otro_dentista.",'".$sd_nom_otro_dent."',".$sd_sangran_encias.",".$sd_cambios_temp.",".$sd_hinch_boca.",".$sd_anestesia.",".$sd_alergico.",'".$sd_med_alergia."',".$sd_dol_cabeza.",".$sd_dol_cuello.",".$sd_dol_oidos.",".$sd_mareos.")";
		
			exeQuery($link, $qry);
			///echo '<br>'.$qry.'<br>';
			
			/// Agregar datos de signos
			/*
			unset($qry);
			usleep($unSec);			
			$qry="INSERT INTO `geadental_hcc_signos` (		id_paciente,fecha_consulta,ta,glucosa) VALUES(
			".$id_paciente.",NOW(),'".$signos_ta."','".$signos_glucosa."')";
					
			exeQuery($link, $qry);
			///echo '<br>'.$qry.'<br>';
			*/
			
			/// Enviar correo de paciente creado (pend)

			///die();
			die("<script>location.href = 'pacientes.php'</script>");

		
		}else{
		  echo '<div class="alert alert-warning">';
			echo '<p data-dismiss="alert">Error al tratar de obtener ID de paciente: '.$msg.'. Favor de contactar al administrador. Qry: '.$qry.'</p>';
			echo '<button><a href="pacientes.php" ></a>Regresar</button>';
		  echo '</div>';
		  die();
		}
		
		
		
		
	}
	///agregarcita
	if(isset($_POST['agregarcita'])){
		(int)$id_med = htmlspecialchars(trim($_POST['id_med'])); 
		///id_paciente_txt
		if(htmlspecialchars(trim($_POST['nombre_pac'])) == 0){
			$id_paciente = $_POST['nombre_pac_sel'];
		}else{
			$id_paciente = htmlspecialchars(trim($_POST['id_paciente_txt']));
		}
		
		$nom_paciente_sinexp = htmlspecialchars(trim($_POST['nom_paciente_sinexp']));
		if($nom_paciente_sinexp != "")
			$id_paciente = 0;
		
		$tel_paciente = htmlspecialchars(trim($_POST['tel_paciente']));
		
		///$nombre_pac_sel = $_POST['nombre_pac_sel'];

		$rawdate1 = htmlentities($_POST['fecha_cita']);
		$fecha_cita = date('Y-m-d', strtotime($rawdate1));

		$hora_cita = htmlspecialchars(trim($_POST['hora_cita']));
		(int)$sd_motif_vis = $_POST['sd_motif_vis'];
		$hechos_cita = htmlspecialchars(trim($_POST['hechos_cita']));
		$observ_cita = '';///htmlspecialchars(trim($_POST['observ_cita']));
		$ta_cita = '';///htmlspecialchars(trim($_POST['ta_cita']));
		$glucosa_cita = '';///htmlspecialchars(trim($_POST['glucosa_cita']));
		(int)$dx_cita = 0;///$_POST['dx_cita'];
		$reco_med =  '';
		$cita_atendida = 0;

		$qry="INSERT INTO `geadental_citas` (id_medico,nombre,tel_pac_sinexp, id_paciente,fecha_cita,hora_cita,id_motivo_visita,ta,glucosa,hechos,observaciones,id_diagnostico,diagnostico,recomendaciones,
		cita_atendida) VALUES(".$id_med.",'".$nom_paciente_sinexp."','".$tel_paciente."',".$id_paciente.",'".$fecha_cita."','".$hora_cita."',".$sd_motif_vis.",'".$ta_cita."','".$glucosa_cita."','".$hechos_cita."','".$observ_cita."',".$dx_cita.",'','".$reco_med."',"
		.$cita_atendida.")";
				
		
		/*
		$qry="INSERT INTO `geadental_citas` (id_medico,id_paciente,fecha_cita,hora_cita,id_motivo_visita,ta,glucosa,hechos,observaciones,id_diagnostico,diagnostico,recomendaciones,
		cita_atendida) VALUES(".$id_med.",".$id_paciente.",'".$fecha_cita."','".$hora_cita."',".$sd_motif_vis.",'".$ta_cita."','".$glucosa_cita."','".$hechos_cita."','".$observ_cita."',".$dx_cita.",'','".$reco_med."',"
		.$cita_atendida.")";
		*/
		
		///echo '<br>'.$qry.'<br>';
		exeQuery($link, $qry);
		
		/// Enviar correo de cita creada (pend)

		die("<script>location.href = 'citas.php'</script>");
		///die();
	}
	///regcita
	if(isset($_POST['regcita'])){
		$id_cita = htmlspecialchars(trim($_POST['id_cita']));
		$observ_cita = htmlspecialchars(trim($_POST['observ_cita']));
		$ta_cita = htmlspecialchars(trim($_POST['ta_cita']));
		$glucosa_cita = htmlspecialchars(trim($_POST['glucosa_cita']));
		(int)$dx_cita = $_POST['dx_cita'];
		$reco_med =  htmlspecialchars(trim($_POST['reco_cita']));
		$cita_atendida = 1;

		$qry="UPDATE `geadental_citas` SET (observaciones = '".$observ_cita."', ta= '".$ta_cita."', glucosa='".$glucosa_cita."', id_diagnostico='".$dx_cita."', recomendaciones='".$reco_med."', cita_atendida=".$cita_atendida.") WHERE id_cita = ".$id_cita;

		echo '<br>'.$qry.'<br>';
	}  
	
  }

	/*
	//////////////////////////////
	params: link, query
	returns: msj de confirmación de que el query fue correctamente ejejcutado. Usado para inserts y updates
	autor: JM
	fecha: Dic 2019
	//////////////////////////////
	*/
	function exeQuery($link, $qry){
		try {  
			$result1 = mysqli_query($link, $qry);

			///echo '<br>'.$qry.'<br>';			
		}  
		catch(exception $e) {
		  ///echo "ex: ".$e; 
		  echo '<div class="alert alert-warning">';
			echo '<p data-dismiss="alert">Error al tratar de obtener ID de paciente: '.$msg.'. Favor de contactar al administrador</p>';
			echo '<button><a href="pacientes.php" ></a>Regresar</button>';
		  echo '</div>';
		  mysql_close($link);
		  die();
		}
		
		return TRUE;
	}

	/*
	//////////////////////////////
	params: link, nombre, f nacim, telefono
	returns: id de paciente
	autor: JM
	fecha: Dic 2019
	//////////////////////////////
	*/
	function obtenerIdpaciente($link, $nom, $fnac, $tel1_pac){
		try {  
			$qry="SELECT `id_paciente` FROM `geadental_hcc_info_personal` WHERE nombre = '".$nom."' AND fecha_de_nacimiento = '".$fnac."' AND tel1 = '".$tel1_pac."' LIMIT 1";
			
			$result1 = mysqli_query($link, $qry);
			$row = $result1->fetch_assoc();
			$id_paciente = $row['id_paciente'];
			
			///echo '<br>qry: '.$qry.'<br>id_paciente: '.$id_paciente.'<br>';
			
		}  
		catch(exception $e) {
		  ///echo "ex: ".$e; 
		  $msg='Message: ' .$e->getMessage();
		  
		  echo '<div class="alert alert-warning">';
			echo '<p data-dismiss="alert">Error al tratar de obtener ID de paciente: '.$msg.'. Favor de contactar al administrador</p>';
			echo '<button><a href="pacientes.php" ></a>Regresar</button>';
		  echo '</div>';
		  mysql_close($link);
		  die();
		  
		}
		
		///echo '<br>qry: '.$qry.'<br>id_paciente: '.$id_paciente.'<br>';
		
		return $id_paciente;
	}
	
	/*
	function qryInsert($link, $tbl, $fields, $values){
		$qry = "INSERT INTO `".$tbl."` (".$fields.") VALUES (".$values.")";
		echo $qry;		
		
		try {  
			$result1 = mysqli_query($link, $qry);  
		}  
		catch(exception $e) {
		  echo "ex: ".$e; 
		}
		
	}
	*/
  
  
	function aes_encrypt($plaintext, $password) {
		$method = "AES-256-CBC";
		$key = hash('sha256', $password, true);
		$iv = openssl_random_pseudo_bytes(16);

		$ciphertext = openssl_encrypt($plaintext, $method, $key, OPENSSL_RAW_DATA, $iv);
		$hash = hash_hmac('sha256', $ciphertext . $iv, $key, true);

		return $iv . $hash . $ciphertext;
	}

	function aes_decrypt($ivHashCiphertext, $password) {
		$method = "AES-256-CBC";
		$iv = substr($ivHashCiphertext, 0, 16);
		$hash = substr($ivHashCiphertext, 16, 32);
		$ciphertext = substr($ivHashCiphertext, 48);
		$key = hash('sha256', $password, true);

		if (!hash_equals(hash_hmac('sha256', $ciphertext . $iv, $key, true), $hash)) return null;

		return openssl_decrypt($ciphertext, $method, $key, OPENSSL_RAW_DATA, $iv);
	}

	function hashhed($str, $options){
		return password_hash($str, PASSWORD_BCRYPT, $options);	;
	}
  
?>
