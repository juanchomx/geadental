<?
session_start();
if($_SESSION['loggedin'] == 0)
die("<script>location.href = 'login.php'</script>");
else{
$id_user_type = $_SESSION['id_user_type'];
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>GEA Dental</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">
    <!-- Bootstrap css -->
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Libraries CSS Files -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/modal-video/css/modal-video.min.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">
    <!-- =======================================================
    Theme Name: eStartup
    Theme URL: https://bootstrapmade.com/estartup-bootstrap-landing-page-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
    ======================================================= -->
  </head>
  <body>
    <header id="header" class="header header-hide">
      <div class="container">
        <div id="logo" class="pull-left">
          <!-- <h1><a href="#body" class="scrollto"><span>e</span>Startup</a></h1> -->
          <!-- Uncomment below if you prefer to use an image logo -->
          <a href="#body"><img src="img/logo.png" alt="GEA Dental" title="" height="40" width="40" /></a>
        </div>
        <nav id="nav-menu-container">
          <ul class="nav-menu">
            <li ><a href="index.php">Inicio</a></li>
            <li class="menu-active"> <a href="#">Pacientes</a></li>
            <!--
            <li><a href="#features">Features</a></li>
            <li><a href="#screenshots">Screenshots</a></li>
            <li><a href="#team">Team</a></li>
            <li><a href="#pricing">Pricing</a></li>
            -->
            <li class="menu-has-children"><a href="">Conta</a>
            <ul>
              <li><a href="#">Inventario</a></li>
              <li><a href="#">Balance</a></li>
            </ul>
          </li>
          <!--
          <li><a href="#blog">Blog</a></li>
          <li><a href="#contact">Contact</a></li>
          -->
          <li><a href="login.php?logout=1" class="external"><span class="glyphicon glyphicon-log-out"></span> Salir</a></li>
        </ul>
        </nav><!-- #nav-menu-container -->
      </div>
      </header><!-- #header -->
      <!--==========================
      Features Section
      ============================-->
      <section id="features" class="padd-section text-center wow fadeInUp">
        <div class="container">
          <div class="section-title text-center">
            <h2>Dashboard</h2>
            <p class="separator">(Aquí van gráficas, indicadores y así)</p>
          </div>
        </div>
        <div class="container">
          <div id="lista_pacientes" class="row" align="center">
            <button type="button" data-toggle="modal" data-target="#myModalAgregarPaciente" class="btn btn-primary" >Agregar Paciente</button>
          </div>
        </div>
      </section>
      <!-- Fin de la página-->
      <!-- Modals -->
      <!-- myModalAgregarPaciente -->
      <div id="myModalAgregarPaciente" class="modal fade" role="dialog" align="center">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              
              <form method="post" action="dbcrud1.php">

              <!-- Info personal: ID -->
              <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#ip_ID">Info Personal</button><br>

              <div id="ip_ID" class="collapse">

                  <div class="form-row">
                    <div class="col">
                      <label for="inputAddress">Nombre</label ><!--&nbsp; &nbsp;<label id="msgs" class="warningmsg1" hidden></label><br> -->
                      <input type="text" class="form-control col-sm-8" id="nombre_pac" name="nombre_pac"  placeholder="nombre paciente" required  >
                    </div>
                    <div class="col">
                      <label for="inputAddress">Sexo</label >
                        <select class="form-control" id="sexo_pac" name="sexo_pac" >
                          <option value="2">Masculino</option>
                          <option value="1">Femenino</option>
                        </select>                    
                   </div>
                    <div class="col">
                      <label for="inputAddress">Fecha Nacimiento</label >
                      <input type="date" class="form-control" id="paciente_fnac" name="paciente_fnac" required>  
                   </div>                   
                </div>
              
              </div>
              <!-- Info personal: ID -->

              <!-- Info personal: Contacto -->
              <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#ip_Contacto">Contacto</button><br>

              <div id="ip_Contacto" class="collapse">
                
                  <div class="form-row">
                    <div class="col">
                      <label for="inputAddress">Dirección</label ><!--&nbsp; &nbsp;<label id="msgs" class="warningmsg1" hidden></label><br> -->
                      <input type="text" class="form-control col-sm-6" id="dir_pac" name="dir_pac" placeholder="Av. Coatzacoalcos #4221" required  >
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="col">
                      <label for="inputAddress">Colonia</label >
                      <input type="text" class="form-control col-sm-6" id="dir_col_pac" name="dir_col_pac" placeholder="Felipe Angeles" required  >
                   </div>
                  </div>

                  <div class="form-row">
                    <div class="col">
                      <label for="inputAddress">Tel. Contacto</label >
                      <input type="text" class="form-control  col-sm-6" id="tel1_pac" name="tel1_pac" placeholder="0123456789" required>  
                   </div>
                  </div>
                
                  <div class="form-row">
                    <div class="col">
                      <label for="inputAddress">Nombre Contacto Emergencia</label ><!--&nbsp; &nbsp;<label id="msgs" class="warningmsg1" hidden></label><br> -->
                      <input type="text" class="form-control col-sm-6" id="dir_pac" name="dir_pac" placeholder="" required  >
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="col">
                      <label for="inputAddress">Te. Contacto Emergencia</label >
                      <input type="text" class="form-control col-sm-6" id="dir_col_pac" name="dir_col_pac" placeholder="0123456789" required  >
                   </div>
                  </div>

                  <div class="form-row">
                    <div class="col">
                      <label for="inputAddress">Nombre Quién recomienda</label >
                      <input type="text" class="form-control  col-sm-6" id="tel1_pac" name="tel1_pac" placeholder="" required>  
                   </div>
                  </div>              
              
              </div>
              <!-- Info personal: Contacto -->
                
                
                
                <br><br>
                <button type="submit" class="btn btn-success" id="savedriver" name="savedriver">Agregar Paciente</button>
                <!--- myModalAgregarPaciente --->
              </form>

            </div>            
          </div>
        </div>
      </div>
      <!-- Modals -->
      <!--==========================
      Footer
      ============================-->
      <footer class="footer">
        <div class="container">
        </div>
        <div class="copyrights">
          <div class="container">
            <p>&copy; Copyrights eStartup. All rights reserved.</p>
            <div class="credits">
              <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
              -->
              Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a> Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
          </div>
        </div>
      </footer>
      <!-- JavaScript Libraries -->
      <script src="lib/jquery/jquery.min.js"></script>
      <script src="lib/jquery/jquery-migrate.min.js"></script>
      <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="lib/superfish/hoverIntent.js"></script>
      <script src="lib/superfish/superfish.min.js"></script>
      <script src="lib/easing/easing.min.js"></script>
      <script src="lib/modal-video/js/modal-video.js"></script>
      <script src="lib/owlcarousel/owl.carousel.min.js"></script>
      <script src="lib/wow/wow.min.js"></script>
      <!-- Contact Form JavaScript File -->
      <script src="contactform/contactform.js"></script>
      <!-- Template Main Javascript File -->
      <script src="js/main.js"></script>
    </body>
  </html>