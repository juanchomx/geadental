<?
include("connection.php");
include("gen_functions.php");
session_start();
if($_SESSION['loggedin'] == 0)
die("<script>location.href = 'login.php'</script>");
else{
$iduser = $_SESSION['iduser'];
$name = $_SESSION['name'];
$id_user_type = $_SESSION['id_user_type'];
$idpanel = 1;///Index
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>GEA Dental</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">
    <!-- Bootstrap css -->
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Libraries CSS Files -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/modal-video/css/modal-video.min.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">
    <!-- =======================================================
    Theme Name: eStartup
    Theme URL: https://bootstrapmade.com/estartup-bootstrap-landing-page-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
    ======================================================= -->
  </head>
  <body>
    <header id="header" class="header header-hide">
      <div class="container">
        <div id="logo" class="pull-left">
          <!-- <h1><a href="#body" class="scrollto"><span>e</span>Startup</a></h1> -->
          <!-- Uncomment below if you prefer to use an image logo -->
          <a href="#body"><img src="img/logo.png" alt="GEA Dental" title="" height="40" width="40" /></a>
        </div>
  
  <!--
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="">Inicio</a></li>
          <li><a href="pacientes.php">Pacientes</a></li>
          <li class="menu-has-children"><a href="">Citas y Tratamientos</a>
          <ul>
            <li><a href="citas.php">Citas</a></li>
            <li><a href="#">Tratamientos</a></li>
          </ul>
        </li>
        <li class="menu-has-children"><a href="">Conta</a>
        <ul>
          <li><a href="#">Inventario</a></li>
          <li><a href="#">Balance</a></li>
        </ul>
      </li>
      <li><a href="login.php?logout=1" class="external"><span class="glyphicon glyphicon-log-out"></span> Salir</a></li>
    </ul>
  </nav>
  -->

  <?
    ///Desplegar menu
    $menu = obtenerMenu($link, $id_user_type, $idpanel);
    if($menu != "")
     echo $menu;
  ?>
  
</div>
</header><!-- #header -->
<!--==========================
Features Section
============================-->
<section id="features" class="padd-section text-center wow fadeInUp">
  <div class="container">
    <div class="section-title text-center">
      <h2>Dashboard</h2>
      <p class="separator">(Aquí van gráficas, indicadores y así)</p>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-lg-3">
        <div class="feature-block">
          <img src="img/svg/asteroid.svg" alt="img" class="img-fluid">
          <h4>Pacientes</h4>
          <p></p>
        </div>
      </div>
      <div class="col-md-6 col-lg-3">
        <div class="feature-block">
          <img src="img/svg/design-tool.svg" alt="img" class="img-fluid">
          <h4>Conta</h4>
          <p></p>
        </div>
      </div>
      <div class="col-md-6 col-lg-3">
        <div class="feature-block">
          <img src="img/svg/cloud.svg" alt="img" class="img-fluid">
          <h4>Inventario</h4>
          <p></p>
          <a href="#"></a>
        </div>
      </div>
    </div>
  </div>
</section>
<!--==========================
Footer
============================-->
<footer class="footer">
  <div class="container">
  </div>
  <div class="copyrights">
    <div class="container">
      <!-- <p>&copy; Copyrights eStartup. All rights reserved.</p> -->
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
        -->
        <!-- Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a> Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
        Developed by <a href="mailto:jmunoz@syner.info">C-Solutions and Clik Graphics. </a>
      </div>
    </div>
  </div>
</footer>
<!-- JavaScript Libraries -->
<script src="lib/jquery/jquery.min.js"></script>
<script src="lib/jquery/jquery-migrate.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="lib/superfish/hoverIntent.js"></script>
<script src="lib/superfish/superfish.min.js"></script>
<script src="lib/easing/easing.min.js"></script>
<script src="lib/modal-video/js/modal-video.js"></script>
<script src="lib/owlcarousel/owl.carousel.min.js"></script>
<script src="lib/wow/wow.min.js"></script>
<!-- Contact Form JavaScript File -->
<script src="contactform/contactform.js"></script>
<!-- Template Main Javascript File -->
<script src="js/main.js"></script>
</body>
</html>